package com.hee.pea.training.model;

import com.hee.pea.training.constant.ResourceType;

public class Login extends BaseModel {

    public Login() {
        super();
    }

    public Login(ResourceType resourceType, String errorCode) {
        super(resourceType, errorCode);
    }
}
