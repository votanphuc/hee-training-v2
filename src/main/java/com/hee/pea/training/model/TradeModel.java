package com.hee.pea.training.model;

import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

import java.text.SimpleDateFormat;
import java.util.Date;

public class TradeModel extends BaseModel {

    private SimpleStringProperty clientId; //getFrom trade

    private SimpleStringProperty tradeType; //BUY

    private SimpleDoubleProperty stock; //stock

    private SimpleDoubleProperty price; //B Px

    private SimpleIntegerProperty quantity; //Qty

    private SimpleStringProperty orderTime; //getCurrentTime

    public TradeModel(String clientId, Double stock, Double price, Integer quantity) {
        this.clientId = new SimpleStringProperty(clientId);
        this.tradeType = new SimpleStringProperty("BUY");
        this.stock = new SimpleDoubleProperty(stock);
        this.price = new SimpleDoubleProperty(price);
        this.quantity = new SimpleIntegerProperty(quantity);
        this.orderTime = new SimpleStringProperty(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(new Date()));
    }

    public String getClientId() {
        return clientId.get();
    }

    public void setClientId(String clientId) {
        this.clientId.set(clientId);
    }

    public SimpleStringProperty clientIdProperty() {
        return clientId;
    }

    public String getTradeType() {
        return tradeType.get();
    }

    public void setTradeType(String tradeType) {
        this.tradeType.set(tradeType);
    }

    public SimpleStringProperty tradeTypeProperty() {
        return tradeType;
    }

    public double getStock() {
        return stock.get();
    }

    public void setStock(double stock) {
        this.stock.set(stock);
    }

    public SimpleDoubleProperty stockProperty() {
        return stock;
    }

    public double getPrice() {
        return price.get();
    }

    public void setPrice(int price) {
        this.price.set(price);
    }

    public SimpleDoubleProperty priceProperty() {
        return price;
    }

    public int getQuantity() {
        return quantity.get();
    }

    public void setQuantity(int quantity) {
        this.quantity.set(quantity);
    }

    public SimpleIntegerProperty quantityProperty() {
        return quantity;
    }

    public String getOrderTime() {
        return orderTime.get();
    }

    public void setOrderTime(String orderTime) {
        this.orderTime.set(orderTime);
    }

    public SimpleStringProperty orderTimeProperty() {
        return orderTime;
    }
}
