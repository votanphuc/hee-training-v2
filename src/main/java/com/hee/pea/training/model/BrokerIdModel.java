package com.hee.pea.training.model;

import javafx.beans.property.SimpleDoubleProperty;

public class BrokerIdModel extends BaseModel {

    private final SimpleDoubleProperty buyIdTableColumn;

    private final SimpleDoubleProperty sellIdTableColumn;

    private final SimpleDoubleProperty buySellTableColumn;

    public BrokerIdModel(Double buyIdTableColumn, Double sellIdTableColumn, Double buySellTableColumn) {
        this.buyIdTableColumn = new SimpleDoubleProperty(buyIdTableColumn);
        this.sellIdTableColumn = new SimpleDoubleProperty(sellIdTableColumn);
        this.buySellTableColumn = new SimpleDoubleProperty(buySellTableColumn);
    }

    public Double getBuyIdTableColumn() {
        return buyIdTableColumn.get();
    }

    public void setBuyIdTableColumn(Double buyIdTableColumn) {
        this.buyIdTableColumn.set(buyIdTableColumn);
    }

    public Double getSellIdTableColumn() {
        return sellIdTableColumn.get();
    }

    public void setSellIdTableColumn(Double sellIdTableColumn) {
        this.sellIdTableColumn.set(sellIdTableColumn);
    }

    public Double getBuySellTableColumn() {
        return buySellTableColumn.get();
    }

    public void setBuySellTableColumn(Double buySellTableColumn) {
        this.buySellTableColumn.set(buySellTableColumn);
    }
}
