package com.hee.pea.training.model;

import com.hee.pea.training.constant.ResourceType;

public class BaseModel {

    protected String errorCode;

    protected ResourceType resourceType;

    public BaseModel() {
        super();
    }

    public BaseModel(ResourceType resourceType, String errorCode) {
        super();
        this.resourceType = resourceType;
        this.errorCode = errorCode;
    }

    public ResourceType getResourceType() {
        return resourceType;
    }

    public void setResourceType(ResourceType resourceType) {
        this.resourceType = resourceType;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("BaseModel [errorCode=");
        builder.append(errorCode);
        builder.append(", resourceType=");
        builder.append(resourceType);
        builder.append("]");
        return builder.toString();
    }
}
