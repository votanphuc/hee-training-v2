package com.hee.pea.training.model;

import javafx.beans.property.SimpleDoubleProperty;

public class BrokerNameModel extends BaseModel {

    private final SimpleDoubleProperty buyNameTableColumn;

    private final SimpleDoubleProperty sellNameTableColumn;

    public BrokerNameModel(Double buyNameTableColumn, Double sellNameTableColumn) {
        this.buyNameTableColumn = new SimpleDoubleProperty(buyNameTableColumn);
        this.sellNameTableColumn = new SimpleDoubleProperty(sellNameTableColumn);
    }

    public Double getBuyNameTableColumn() {
        return buyNameTableColumn.get();
    }

    public void setBuyNameTableColumn(Double buyNameTableColumn) {
        this.buyNameTableColumn.set(buyNameTableColumn);
    }

    public Double getSellNameTableColumn() {
        return sellNameTableColumn.get();
    }

    public void setSellNameTableColumn(Double sellNameTableColumn) {
        this.sellNameTableColumn.set(sellNameTableColumn);
    }
}
