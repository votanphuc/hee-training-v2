package com.hee.pea.training.view.impl;

import com.hee.pea.training.view.AbstractView;
import com.hee.pea.training.view.MenuBarView;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.Pane;

public class MenuBarViewImpl extends AbstractView implements MenuBarView {

    private FlowPane topBarFlowPane;

    private MenuBar menuBar;

    private Menu systemMenu;

    private MenuItem menuItem;

    public MenuBarViewImpl() {
        topBarFlowPane = new FlowPane();
        topBarFlowPane.setHgap(10);
        topBarFlowPane.setVgap(10);

        menuItem = new MenuItem("Exit");
        systemMenu = new Menu("System");
        systemMenu.getItems().add(menuItem);
        menuBar = new MenuBar();
        menuBar.getMenus().add(systemMenu);

        topBarFlowPane.getChildren().add(menuBar);
    }

    @Override
    public Pane getView() {
        return this;
    }

    @Override
    public Pane getPane(){
        return topBarFlowPane;
    }

    @Override
    public MenuBar getMenuBar() {
        return menuBar;
    }

    @Override
    public Menu getSystemMenu() {
        return systemMenu;
    }

    @Override
    public MenuItem getMenuItem() {
        return menuItem;
    }
}
