package com.hee.pea.training.view.impl;

import com.hee.pea.training.presenter.*;
import com.hee.pea.training.view.*;
import com.hee.pea.training.viewmodel.*;
import javafx.geometry.Orientation;
import javafx.scene.control.SplitPane;
import javafx.scene.layout.Pane;

public class MainViewImpl extends AbstractView implements MainView {

    private TradePresenter tradePresenter;
    private TradeView tradeView;

    private MenuBarPresenter menuBarPresenter;
    private MenuBarView menuBarView;

    private StatusBarPresenter statusBarPresenter;
    private StatusBarView statusBarView;

    private OrderTicketPresenter orderTicketPresenter;
    private OrderTicketView orderTicketView;

    private BrokerPresenter brokerPresenter;
    private BrokerView brokerView;

    public MainViewImpl() {
        tradePresenter = new TradePresenter(new TradeViewImpl(), new TradeViewModel());
        tradeView = tradePresenter.getView();

        menuBarPresenter = new MenuBarPresenter(new MenuBarViewImpl(), new MenuBarViewModel());
        menuBarView = menuBarPresenter.getView();

        statusBarPresenter = new StatusBarPresenter(new StatusBarViewImpl(), new StatusBarViewModel());
        statusBarView = statusBarPresenter.getView();

        orderTicketPresenter = new OrderTicketPresenter(new OrderTicketViewImpl(), new OrderTicketViewModel());
        orderTicketView = orderTicketPresenter.getView();

        brokerPresenter = new BrokerPresenter(new BrokerViewImpl(), new BrokerViewModel());
        brokerView = brokerPresenter.getView();

        SplitPane dataInfoSplitPane = new SplitPane(orderTicketView.getPane(), brokerView.getPane());
        dataInfoSplitPane.setOrientation(Orientation.VERTICAL);

        SplitPane formSplitPane = new SplitPane();
        formSplitPane.setDividerPositions(0.23, 1);
        formSplitPane.getItems().addAll(tradeView.getPane(), dataInfoSplitPane);

        this.setTop(menuBarView.getPane());
        this.setCenter(formSplitPane);
        this.setBottom(statusBarView.getPane());

    }

    @Override
    public Pane getView() {
        return this;
    }

    @Override
    public TradePresenter getTradePresenter() {
        return tradePresenter;
    }

    @Override
    public TradeView getTradeView() {
        return tradeView;
    }

    @Override
    public MenuBarPresenter getMenuBarPresenter() {
        return menuBarPresenter;
    }

    @Override
    public MenuBarView getMenuBarView() {
        return menuBarView;
    }

    @Override
    public StatusBarPresenter getStatusBarPresenter() {
        return statusBarPresenter;
    }

    @Override
    public StatusBarView getStatusBarView() {
        return statusBarView;
    }

    @Override
    public OrderTicketPresenter getOrderTicketPresenter() {
        return orderTicketPresenter;
    }

    @Override
    public OrderTicketView getOrderTicketView() {
        return orderTicketView;
    }

    @Override
    public BrokerPresenter getBrokerPresenter() {
        return brokerPresenter;
    }

    @Override
    public BrokerView getBrokerView() {
        return brokerView;
    }
}
