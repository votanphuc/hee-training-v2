package com.hee.pea.training.view;

import com.hee.pea.training.component.raw.NumberTextField;
import javafx.scene.control.*;

public interface TradeView extends BaseView {

    Label getAccountLabel();

    void setAccountLabel(Label accountLabel);

    TextField getAccountTextField();

    void setAccountTextField(TextField accountTextField);

    Label getAccountNameLabel();

    void setAccountNameLabel(Label accountNameLabel);

    CheckBox getAccountCheckBox();

    void setAccountCheckBox(CheckBox accountCheckBox);

    Label getStockLabel();

    void setStockLabel(Label stockLabel);

    TextField getStockTextField();

    void setStockTextField(TextField stockTextField);

    CheckBox getStockCheckBox();

    void setStockCheckBox(CheckBox stockCheckBox);

    Label getTypeLabel();

    void setTypeLabel(Label typeLabel);

    ComboBox<String> getTypeComboBox();

    void setTypeComboBox(ComboBox<String> typeComboBox);

    CheckBox getTypeCheckBox();

    void setTypeCheckBox(CheckBox typeCheckBox);

    Label getbPxLabel();

    void setbPxLabel(Label bPxLabel);

    TextField getbPxTextField();

    void setbPxTextField(NumberTextField bPxTextField);

    Button getbPxPlusButton();

    void setbPxPlusButton(Button bPxPlusButton);

    Button getbPxMinusButton();

    void setbPxMinusButton(Button bPxMinusButton);

    CheckBox getbPxCheckBox();

    void setbPxCheckBox(CheckBox bPxCheckBox);

    Label getQtyLabel();

    void setQtyLabel(Label qtyLabel);

    TextField getQtyTextField();

    void setQtyTextField(NumberTextField qtyTextField);

    Button getQtyPlusButton();

    void setQtyPlusButton(Button qtyPlusButton);

    Button getQtyMinusButton();

    void setQtyMinusButton(Button qtyMinusButton);

    CheckBox getQtyCheckBox();

    void setQtyCheckBox(CheckBox qtyCheckBox);

    Label getPpLabel();

    void setPpLabel(Label ppLabel);

    Label getPpValueLabel();

    void setPpValueLabel(Label ppValueLabel);

    Label getTotalLabel();

    void setTotalLabel(Label totalLabel);

    Label getTotalValueLabel();

    void setTotalValueLabel(Label totalValueLabel);

    Label getOtoLabel();

    void setOtoLabel(Label otoLabel);

    Toggle getOtoToggle();

    void setOtoToggle(Toggle otoToggle);

    Button getBuyButton();

    void setBuyButton(Button buyButton);

    Button getClearButton();

    void setClearButton(Button clearButton);
}
