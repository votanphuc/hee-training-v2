package com.hee.pea.training.view;

import javafx.scene.control.*;

public interface BrokerView extends BaseView {

    TextField getStockTextField();

    TabPane getTabPane();

    Tab getBrokerIdTab();

    Tab getBrokerNameTab();

    TableView getBrokerIdTableView();

    TableView getBrokerNameTableView();

    TableColumn getBuyIdTableColumn();

    TableColumn getSellIdTableColumn();

    TableColumn getBuySellTableColumn();

    TableColumn getBuyNameTableColumn();

    TableColumn getSellNameTableColumn();
}
