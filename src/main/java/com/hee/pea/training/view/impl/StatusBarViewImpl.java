package com.hee.pea.training.view.impl;

import com.hee.pea.training.view.AbstractView;
import com.hee.pea.training.view.StatusBarView;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;

public class StatusBarViewImpl extends AbstractView implements StatusBarView {

    private Label loginIdLabel;
    private Label loginIdValueLabel;
    private Label companyNameLabel;

    public StatusBarViewImpl() {
        loginIdLabel = new Label("Login ID: ");
        loginIdLabel.setPadding( new Insets(5,5,5,5));
        loginIdValueLabel = new Label("heevn");
        HBox loginInfoHBox = new HBox(loginIdLabel, loginIdValueLabel);

        companyNameLabel = new Label("Hee Solutions Ltd");
        companyNameLabel.setPadding( new Insets(5,5,5,5));

        this.setLeft(loginInfoHBox);
        this.setRight(companyNameLabel);
    }

    @Override
    public Pane getView() {
        return this;
    }

}
