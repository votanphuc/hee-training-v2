package com.hee.pea.training.view;

import com.hee.pea.training.presenter.*;
import javafx.scene.control.SplitPane;

public interface MainView extends BaseView {

    TradePresenter getTradePresenter();

    TradeView getTradeView();

    MenuBarPresenter getMenuBarPresenter();

    MenuBarView getMenuBarView();

    StatusBarPresenter getStatusBarPresenter();

    StatusBarView getStatusBarView();

    OrderTicketPresenter getOrderTicketPresenter();

    OrderTicketView getOrderTicketView();

    BrokerPresenter getBrokerPresenter();

    BrokerView getBrokerView();
}
