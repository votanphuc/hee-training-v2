package com.hee.pea.training.view;

import com.hee.pea.training.model.TradeModel;
import javafx.scene.control.*;
import javafx.scene.layout.Pane;

public interface OrderTicketView extends BaseView {
    void initTableView();

    @Override
    Pane getView();

    TextField getAccountTextField();

    void setAccountTextField(TextField accountTextField);

    Label getAccountLabel();

    void setAccountLabel(Label accountLabel);

    CheckBox getAccountCheckBox();

    void setAccountCheckBox(CheckBox accountCheckBox);

    RadioButton getAllRadioButton();

    void setAllRadioButton(RadioButton allRadioButton);

    RadioButton getCompletedlRadioButton();

    void setCompletedlRadioButton(RadioButton completedlRadioButton);

    RadioButton getOutstandingRadioButton();

    void setOutstandingRadioButton(RadioButton outstandingRadioButton);

    RadioButton getRejectedOrderRadioButton();

    void setRejectedOrderRadioButton(RadioButton rejectedOrderRadioButton);

    RadioButton getCancelledShowRadioButton();

    void setCancelledShowRadioButton(RadioButton cancelledShowRadioButton);

    CheckBox getStatusCheckBox();

    void setStatusCheckBox(CheckBox stausCheckBox);

    TableView<TradeModel> getTableView();

    void setTableView(TableView<TradeModel> tableView);

    TableColumn getClientIdTableColumn();

    void setClientIdTableColumn(TableColumn clientIdTableColumn);

    TableColumn getStatusTableColumn();

    void setStatusTableColumn(TableColumn statusTableColumn);

    TableColumn getOrderIdTableColumn();

    void setOrderIdTableColumn(TableColumn orderIdTableColumn);

    TableColumn getBuySellTableColumn();

    void setBuySellTableColumn(TableColumn buySellTableColumn);

    TableColumn getCodeTableColumn();

    void setCodeTableColumn(TableColumn codeTableColumn);

    TableColumn getPriceTableColumn();

    void setPriceTableColumn(TableColumn priceTableColumn);

    TableColumn getAverageTableColumn();

    void setAverageTableColumn(TableColumn averageTableColumn);

    TableColumn getQuantityTableColumn();

    void setQuantityTableColumn(TableColumn quantityTableColumn);

    TableColumn getTotalAmountTableColumn();

    void setTotalAmountTableColumn(TableColumn totalAmountTableColumn);

    TableColumn getOrderTimeTableColumn();

    void setOrderTimeTableColumn(TableColumn orderTimeTableColumn);
}
