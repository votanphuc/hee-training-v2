package com.hee.pea.training.view.impl;

import com.hee.pea.training.component.raw.NumberTextField;
import com.hee.pea.training.view.AbstractView;
import com.hee.pea.training.view.TradeView;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class TradeViewImpl extends AbstractView implements TradeView {

    private static final Logger LOGGER = LogManager.getLogger(TradeViewImpl.class);

    private GridPane gridPane;

    private Label accountLabel;
    private TextField accountTextField;
    private Label accountNameLabel;
    private CheckBox accountCheckBox;

    private Label stockLabel;
    private TextField stockTextField;
    private CheckBox stockCheckBox;

    private Label typeLabel;
    private ComboBox<String> typeComboBox;
    private CheckBox typeCheckBox;

    private Label bPxLabel;
    private NumberTextField bPxTextField;
    private Button bPxPlusButton;
    private Button bPxMinusButton;
    private CheckBox bPxCheckBox;

    private Label qtyLabel;
    private NumberTextField qtyTextField;
    private Button qtyPlusButton;
    private Button qtyMinusButton;
    private CheckBox qtyCheckBox;

    private Label ppLabel;
    private Label ppValueLabel;

    private Label totalLabel;
    private Label totalValueLabel;

    private Label otoLabel;
    private Toggle otoToggle;

    private Button buyButton;
    private Button clearButton;

    public TradeViewImpl() {
        gridPane = new GridPane();
        gridPane.setAlignment(Pos.TOP_LEFT);
        gridPane.setHgap(10);
        gridPane.setVgap(10);
        gridPane.setPadding(new Insets(5, 5, 0, 10));
        Background background = new Background(new BackgroundFill(Color.LIGHTBLUE, CornerRadii.EMPTY, Insets.EMPTY));
        gridPane.setBackground(background);

        accountLabel = new Label("Account");
        accountTextField = new TextField();
        accountTextField.setPrefWidth(90);
        accountNameLabel = new Label("heevn");
        HBox accountHBox = new HBox(accountTextField, accountNameLabel);
        accountCheckBox = new CheckBox();
        gridPane.add(accountLabel, 0, 0);
        gridPane.add(accountHBox, 1, 0);
        gridPane.add(accountCheckBox, 2, 0);

        stockLabel = new Label("Stock");
        stockTextField = new NumberTextField(NumberTextField.NumberTextFieldType.CURRENCY);
        stockCheckBox = new CheckBox();
        HBox stockHBox = new HBox(stockTextField);
        gridPane.add(stockLabel, 0, 1);
        gridPane.add(stockHBox, 1, 1);
        gridPane.add(stockCheckBox, 2, 1);

        typeLabel = new Label("Type");
        typeComboBox = new ComboBox<>();
        typeComboBox.getItems().addAll("Enhanced Limit Order", "Special Limit Order", "Manual");
        typeComboBox.getSelectionModel().selectFirst();
        typeCheckBox = new CheckBox();
        HBox typeHBox = new HBox(typeComboBox);
        gridPane.add(typeLabel, 0, 2);
        gridPane.add(typeHBox, 1, 2);
        gridPane.add(typeCheckBox, 2, 2);

        bPxLabel = new Label("B Px");
        bPxTextField = new NumberTextField(NumberTextField.NumberTextFieldType.CURRENCY);
        bPxMinusButton = new Button("-");
        bPxPlusButton = new Button("+");
        bPxCheckBox = new CheckBox();
        HBox bPxHBox = new HBox(bPxTextField, bPxMinusButton, bPxPlusButton);
        gridPane.add(bPxLabel, 0, 3);
        gridPane.add(bPxHBox, 1, 3);
        gridPane.add(bPxCheckBox, 2, 3);

        qtyLabel = new Label("Qty");
        qtyTextField = new NumberTextField(NumberTextField.NumberTextFieldType.QUANTITY);
        qtyMinusButton = new Button("-");
        qtyPlusButton = new Button("+");
        qtyCheckBox = new CheckBox();
        HBox qtyHBox = new HBox(qtyTextField, qtyMinusButton, qtyPlusButton);
        gridPane.add(qtyLabel, 0, 4);
        gridPane.add(qtyHBox, 1, 4);
        gridPane.add(qtyCheckBox, 2, 4);

        ppLabel = new Label("pp");
        ppValueLabel = new Label("989,988.61");
        gridPane.add(ppLabel, 0, 5);
        gridPane.add(ppValueLabel, 1, 5);

        totalLabel = new Label("Total");
        totalValueLabel = new Label("5,123.61");
        gridPane.add(totalLabel, 0, 6);
        gridPane.add(totalValueLabel, 1, 6);

        otoLabel = new Label("OTO");
        otoToggle = new ToggleButton();
        gridPane.add(otoLabel, 2, 7);
        //TODO: addToggle

        buyButton = new Button("Buy");
        clearButton = new Button("Clear");
        buyButton.setPrefWidth(50);
        clearButton.setPrefWidth(50);
        HBox ctrlHBox = new HBox(10, buyButton, clearButton);
        ctrlHBox.setAlignment(Pos.BOTTOM_CENTER);
        gridPane.add(ctrlHBox, 1, 10);

        this.setCenter(gridPane);
    }

    @Override
    public Pane getView() {
        return this;
    }

    @Override
    public Pane getPane() {
        return gridPane;
    }

    @Override
    public Label getAccountLabel() {
        return accountLabel;
    }

    @Override
    public void setAccountLabel(Label accountLabel) {
        this.accountLabel = accountLabel;
    }

    @Override
    public TextField getAccountTextField() {
        return accountTextField;
    }

    @Override
    public void setAccountTextField(TextField accountTextField) {
        this.accountTextField = accountTextField;
    }

    @Override
    public Label getAccountNameLabel() {
        return accountNameLabel;
    }

    @Override
    public void setAccountNameLabel(Label accountNameLabel) {
        this.accountNameLabel = accountNameLabel;
    }

    @Override
    public CheckBox getAccountCheckBox() {
        return accountCheckBox;
    }

    @Override
    public void setAccountCheckBox(CheckBox accountCheckBox) {
        this.accountCheckBox = accountCheckBox;
    }

    @Override
    public Label getStockLabel() {
        return stockLabel;
    }

    @Override
    public void setStockLabel(Label stockLabel) {
        this.stockLabel = stockLabel;
    }

    @Override
    public TextField getStockTextField() {
        return stockTextField;
    }

    @Override
    public void setStockTextField(TextField stockTextField) {
        this.stockTextField = stockTextField;
    }

    @Override
    public CheckBox getStockCheckBox() {
        return stockCheckBox;
    }

    @Override
    public void setStockCheckBox(CheckBox stockCheckBox) {
        this.stockCheckBox = stockCheckBox;
    }

    @Override
    public Label getTypeLabel() {
        return typeLabel;
    }

    @Override
    public void setTypeLabel(Label typeLabel) {
        this.typeLabel = typeLabel;
    }

    @Override
    public ComboBox<String> getTypeComboBox() {
        return typeComboBox;
    }

    @Override
    public void setTypeComboBox(ComboBox<String> typeComboBox) {
        this.typeComboBox = typeComboBox;
    }

    @Override
    public CheckBox getTypeCheckBox() {
        return typeCheckBox;
    }

    @Override
    public void setTypeCheckBox(CheckBox typeCheckBox) {
        this.typeCheckBox = typeCheckBox;
    }

    @Override
    public Label getbPxLabel() {
        return bPxLabel;
    }

    @Override
    public void setbPxLabel(Label bPxLabel) {
        this.bPxLabel = bPxLabel;
    }

    @Override
    public TextField getbPxTextField() {
        return bPxTextField;
    }

    @Override
    public void setbPxTextField(NumberTextField bPxTextField) {
        this.bPxTextField = bPxTextField;
    }

    @Override
    public Button getbPxPlusButton() {
        return bPxPlusButton;
    }

    @Override
    public void setbPxPlusButton(Button bPxPlusButton) {
        this.bPxPlusButton = bPxPlusButton;
    }

    @Override
    public Button getbPxMinusButton() {
        return bPxMinusButton;
    }

    @Override
    public void setbPxMinusButton(Button bPxMinusButton) {
        this.bPxMinusButton = bPxMinusButton;
    }

    @Override
    public CheckBox getbPxCheckBox() {
        return bPxCheckBox;
    }

    @Override
    public void setbPxCheckBox(CheckBox bPxCheckBox) {
        this.bPxCheckBox = bPxCheckBox;
    }

    @Override
    public Label getQtyLabel() {
        return qtyLabel;
    }

    @Override
    public void setQtyLabel(Label qtyLabel) {
        this.qtyLabel = qtyLabel;
    }

    @Override
    public TextField getQtyTextField() {
        return qtyTextField;
    }

    @Override
    public void setQtyTextField(NumberTextField qtyTextField) {
        this.qtyTextField = qtyTextField;
    }

    @Override
    public Button getQtyPlusButton() {
        return qtyPlusButton;
    }

    @Override
    public void setQtyPlusButton(Button qtyPlusButton) {
        this.qtyPlusButton = qtyPlusButton;
    }

    @Override
    public Button getQtyMinusButton() {
        return qtyMinusButton;
    }

    @Override
    public void setQtyMinusButton(Button qtyMinusButton) {
        this.qtyMinusButton = qtyMinusButton;
    }

    @Override
    public CheckBox getQtyCheckBox() {
        return qtyCheckBox;
    }

    @Override
    public void setQtyCheckBox(CheckBox qtyCheckBox) {
        this.qtyCheckBox = qtyCheckBox;
    }

    @Override
    public Label getPpLabel() {
        return ppLabel;
    }

    @Override
    public void setPpLabel(Label ppLabel) {
        this.ppLabel = ppLabel;
    }

    @Override
    public Label getPpValueLabel() {
        return ppValueLabel;
    }

    @Override
    public void setPpValueLabel(Label ppValueLabel) {
        this.ppValueLabel = ppValueLabel;
    }

    @Override
    public Label getTotalLabel() {
        return totalLabel;
    }

    @Override
    public void setTotalLabel(Label totalLabel) {
        this.totalLabel = totalLabel;
    }

    @Override
    public Label getTotalValueLabel() {
        return totalValueLabel;
    }

    @Override
    public void setTotalValueLabel(Label totalValueLabel) {
        this.totalValueLabel = totalValueLabel;
    }

    @Override
    public Label getOtoLabel() {
        return otoLabel;
    }

    @Override
    public void setOtoLabel(Label otoLabel) {
        this.otoLabel = otoLabel;
    }

    @Override
    public Toggle getOtoToggle() {
        return otoToggle;
    }

    @Override
    public void setOtoToggle(Toggle otoToggle) {
        this.otoToggle = otoToggle;
    }

    @Override
    public Button getBuyButton() {
        return buyButton;
    }

    @Override
    public void setBuyButton(Button buyButton) {
        this.buyButton = buyButton;
    }

    @Override
    public Button getClearButton() {
        return clearButton;
    }

    @Override
    public void setClearButton(Button clearButton) {
        this.clearButton = clearButton;
    }
}
