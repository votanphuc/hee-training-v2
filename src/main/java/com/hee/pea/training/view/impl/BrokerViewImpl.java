package com.hee.pea.training.view.impl;

import com.hee.pea.training.model.BrokerIdModel;
import com.hee.pea.training.model.BrokerNameModel;
import com.hee.pea.training.view.AbstractView;
import com.hee.pea.training.view.BrokerView;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

public class BrokerViewImpl extends AbstractView implements BrokerView {

    private TextField stockTextField;

    private TabPane tabPane;
    private Tab brokerIdTab;
    private Tab brokerNameTab;

    private TableView<BrokerIdModel> brokerIdTableView;
    private TableView<BrokerNameModel> brokerNameTableView;

    private TableColumn buyIdTableColumn;
    private TableColumn sellIdTableColumn;
    private TableColumn buySellTableColumn;

    private TableColumn buyNameTableColumn;
    private TableColumn sellNameTableColumn;

    public BrokerViewImpl() {
        stockTextField = new TextField();
        stockTextField.setPrefWidth(90);
        HBox stockVBox = new HBox(stockTextField);

        buyIdTableColumn = new TableColumn("Buy");
        sellIdTableColumn = new TableColumn("Sell");
        buySellTableColumn = new TableColumn("Buy - Sell");
        buyIdTableColumn.setCellValueFactory(new PropertyValueFactory<BrokerIdModel, Double>("buyIdTableColumn"));
        sellIdTableColumn.setCellValueFactory(new PropertyValueFactory<BrokerIdModel, Double>("sellIdTableColumn"));
        buySellTableColumn.setCellValueFactory(new PropertyValueFactory<BrokerIdModel, Double>("buySellTableColumn"));

        brokerIdTableView = new TableView();
        brokerIdTableView.getColumns().add(buyIdTableColumn);
        brokerIdTableView.getColumns().add(sellIdTableColumn);
        brokerIdTableView.getColumns().add(buySellTableColumn);
        brokerIdTab = new Tab("Broker ID");
        brokerIdTab.setContent(brokerIdTableView);

        buyNameTableColumn = new TableColumn("Buy");
        sellNameTableColumn = new TableColumn("Sell");
        buyNameTableColumn.setCellValueFactory(new PropertyValueFactory<BrokerNameModel, Double>("buyNameTableColumn"));
        sellNameTableColumn.setCellValueFactory(new PropertyValueFactory<BrokerNameModel, Double>("sellNameTableColumn"));

        brokerNameTableView = new TableView();
        brokerNameTableView.getColumns().add(buyNameTableColumn);
        brokerNameTableView.getColumns().add(sellNameTableColumn);
        brokerNameTab = new Tab("Broker Name");
        brokerNameTab.setContent(brokerNameTableView);

        tabPane = new TabPane();
        tabPane.getTabs().add(brokerIdTab);
        tabPane.getTabs().add(brokerNameTab);

        this.setCenter(new VBox(stockVBox, tabPane));
        this.getStylesheets().add(getClass().getResource("/css/table-broker.css").toExternalForm());
    }

    @Override
    public Pane getView() {
        return this;
    }

    @Override
    public TextField getStockTextField() {
        return stockTextField;
    }

    @Override
    public TabPane getTabPane() {
        return tabPane;
    }

    @Override
    public Tab getBrokerIdTab() {
        return brokerIdTab;
    }

    @Override
    public Tab getBrokerNameTab() {
        return brokerNameTab;
    }

    @Override
    public TableView getBrokerIdTableView() {
        return brokerIdTableView;
    }

    @Override
    public TableView getBrokerNameTableView() {
        return brokerNameTableView;
    }

    @Override
    public TableColumn getBuyIdTableColumn() {
        return buyIdTableColumn;
    }

    @Override
    public TableColumn getSellIdTableColumn() {
        return sellIdTableColumn;
    }

    @Override
    public TableColumn getBuySellTableColumn() {
        return buySellTableColumn;
    }

    @Override
    public TableColumn getBuyNameTableColumn() {
        return buyNameTableColumn;
    }

    @Override
    public TableColumn getSellNameTableColumn() {
        return sellNameTableColumn;
    }
}
