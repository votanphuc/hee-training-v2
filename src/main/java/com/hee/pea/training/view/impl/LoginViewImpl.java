package com.hee.pea.training.view.impl;

import com.hee.pea.training.view.AbstractView;
import com.hee.pea.training.view.LoginView;
import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.layout.*;

public class LoginViewImpl extends AbstractView implements LoginView {

    private Label loginIdLabel;

    private Label loginPwLabel;

    private Label languageLabel;

    private TextField loginIdTextField;

    private PasswordField loginPwTextField;

    private ComboBox languageComboBox;

    private CheckBox saveIdCheckBox;

    private CheckBox savePwCheckBox;

    private Button loginButton;

    private Hyperlink forgetPwButton;

    public LoginViewImpl() {
        GridPane gridPane = new GridPane();

        loginIdLabel = new Label();
        loginPwLabel = new Label();
        languageLabel = new Label();
        loginIdTextField = new TextField();
        loginPwTextField = new PasswordField();
        languageComboBox = new ComboBox<>();
        saveIdCheckBox = new CheckBox();
        savePwCheckBox = new CheckBox();
        loginButton = new Button();
        forgetPwButton = new Hyperlink();

        loginIdLabel.setId("loginIdLabel");
        loginPwLabel.setId("loginPwLabel");
        languageLabel.setId("languageLabel");
        loginIdTextField.setId("loginIdTextField");
        loginPwTextField.setId("loginPwTextField");
        languageComboBox.setId("languageComboBox");
        saveIdCheckBox.setId("saveIdCheckBox");
        savePwCheckBox.setId("savePwCheckBox");
        loginButton.setId("loginButton");
        forgetPwButton.setId("forgetPwButton");

        GridPane.setColumnIndex(loginIdLabel, 0);
        GridPane.setRowIndex(loginIdLabel, 0);

        GridPane.setColumnIndex(loginIdTextField, 1);
        GridPane.setRowIndex(loginIdTextField, 0);

        GridPane.setColumnIndex(loginPwLabel, 0);
        GridPane.setRowIndex(loginPwLabel, 1);

        GridPane.setColumnIndex(loginPwTextField, 1);
        GridPane.setRowIndex(loginPwTextField, 1);

        GridPane.setColumnIndex(languageLabel, 0);
        GridPane.setRowIndex(languageLabel, 2);

        GridPane.setColumnIndex(languageComboBox, 1);
        GridPane.setRowIndex(languageComboBox, 2);

        GridPane.setColumnIndex(saveIdCheckBox, 1);
        GridPane.setRowIndex(saveIdCheckBox, 3);

        GridPane.setColumnIndex(savePwCheckBox, 1);
        GridPane.setRowIndex(savePwCheckBox, 4);

        GridPane.setColumnIndex(forgetPwButton, 0);
        GridPane.setRowIndex(forgetPwButton, 4);

        GridPane.setColumnIndex(loginButton, 1);
        GridPane.setRowIndex(loginButton, 7);

        GridPane.setMargin(loginButton, new Insets(10, 0, 0, 0));
        GridPane.setMargin(forgetPwButton, new Insets(0, 0, 0, -2));

        gridPane.getChildren().add(loginIdLabel);
        gridPane.getChildren().add(loginIdTextField);
        gridPane.getChildren().add(loginPwLabel);
        gridPane.getChildren().add(loginPwTextField);
        gridPane.getChildren().add(languageLabel);
        gridPane.getChildren().add(languageComboBox);
        gridPane.getChildren().add(saveIdCheckBox);
        gridPane.getChildren().add(savePwCheckBox);
        gridPane.getChildren().add(forgetPwButton);
        gridPane.getChildren().add(loginButton);

        ColumnConstraints columnConstraints0 = new ColumnConstraints();
        columnConstraints0.setHgrow(Priority.SOMETIMES);
        columnConstraints0.setMinWidth(10.0);
        columnConstraints0.setPrefWidth(200.0);

        ColumnConstraints columnConstraints1 = new ColumnConstraints();
        columnConstraints1.setHgrow(Priority.SOMETIMES);
        columnConstraints1.setMinWidth(10.0);
        columnConstraints1.setPrefWidth(250.0);

        RowConstraints rowConstraints = new RowConstraints();
        rowConstraints.setVgrow(Priority.SOMETIMES);
        rowConstraints.setMinHeight(10.0);
        rowConstraints.setPrefHeight(30.0);

        gridPane.getColumnConstraints().add(columnConstraints0);
        gridPane.getColumnConstraints().add(columnConstraints1);

        gridPane.getRowConstraints().add(rowConstraints);
        gridPane.getRowConstraints().add(rowConstraints);
        gridPane.getRowConstraints().add(rowConstraints);
        gridPane.getRowConstraints().add(rowConstraints);
        gridPane.getRowConstraints().add(rowConstraints);
        gridPane.getRowConstraints().add(rowConstraints);

        gridPane.setPadding(new Insets(20.0, 20.0, 20.0, 20.0));

        StackPane stackPane = new StackPane();
        BorderPane borderPane = new BorderPane();

        borderPane.setCenter(gridPane);

        stackPane.getChildren().add(borderPane);

        this.setPrefHeight(250);
        this.setPrefWidth(450);
        this.setCenter(stackPane);

        loginIdLabel.getStyleClass().add("width-L");
        loginPwLabel.getStyleClass().add("width-L");
        languageLabel.getStyleClass().add("width-L");
        loginIdTextField.getStyleClass().add("width-L");
        loginPwTextField.getStyleClass().add("width-L");
        languageComboBox.getStyleClass().add("comboBox-L");
        loginButton.getStyleClass().add("width-XS");
        forgetPwButton.setStyle("-fx-text-fill: #0096c9;");
    }

    @Override
    public Label getLoginIdLabel() {
        return loginIdLabel;
    }

    @Override
    public Label getLoginPwLabel() {
        return loginPwLabel;
    }

    @Override
    public Label getLanguageLabel() {
        return languageLabel;
    }

    @Override
    public TextField getLoginIdTextField() {
        return loginIdTextField;
    }

    @Override
    public PasswordField getLoginPwtextField() {
        return loginPwTextField;
    }

    @Override
    public ComboBox getLanguageComboBox() {
        return languageComboBox;
    }

    @Override
    public CheckBox getSaveIdCheckBox() {
        return saveIdCheckBox;
    }

    @Override
    public CheckBox getSavePwCheckBox() {
        return savePwCheckBox;
    }

    @Override
    public Button getLoginButton() {
        return loginButton;
    }

    @Override
    public Hyperlink getForgetPwButton() {
        return forgetPwButton;
    }

    @Override
    public BorderPane getView() {
        return this;
    }
}
