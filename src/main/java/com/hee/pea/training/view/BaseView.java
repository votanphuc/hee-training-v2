package com.hee.pea.training.view;

import javafx.scene.layout.Pane;

public interface BaseView {

    Pane getView();

    default Pane getPane() {
        return getView();
    }
}