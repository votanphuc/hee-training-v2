package com.hee.pea.training.view.impl;

import com.hee.pea.training.model.TradeModel;
import com.hee.pea.training.view.AbstractView;
import com.hee.pea.training.view.OrderTicketView;
import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

public class OrderTicketViewImpl extends AbstractView implements OrderTicketView {

    private TextField accountTextField;
    private Label accountLabel;
    private CheckBox accountCheckBox;

    private RadioButton allRadioButton;
    private RadioButton completedlRadioButton;
    private RadioButton outstandingRadioButton;
    private RadioButton rejectedOrderRadioButton;
    private RadioButton cancelledShowRadioButton;
    private CheckBox statusCheckBox;

    private TableView<TradeModel> tableView;
    private TableColumn clientIdTableColumn;
    private TableColumn statusTableColumn;
    private TableColumn orderIdTableColumn;
    private TableColumn buySellTableColumn;
    private TableColumn codeTableColumn;
    private TableColumn priceTableColumn;
    private TableColumn averageTableColumn;
    private TableColumn quantityTableColumn;
    private TableColumn totalAmountTableColumn;
    private TableColumn orderTimeTableColumn;

    public OrderTicketViewImpl() {
        accountTextField = new TextField();
        accountTextField.setPrefWidth(90);
        accountLabel = new Label("heevn");
        accountCheckBox = new CheckBox();

        HBox accountHBox = new HBox(10);
        accountHBox.setPadding(new Insets(5));
        accountHBox.getChildren().add(accountTextField);
        accountHBox.getChildren().add(accountLabel);
        accountHBox.getChildren().add(accountCheckBox);

        ToggleGroup group = new ToggleGroup();
        allRadioButton = new RadioButton("All");
        completedlRadioButton = new RadioButton("Completed");
        outstandingRadioButton = new RadioButton("Outstanding");
        rejectedOrderRadioButton = new RadioButton("Rejected Order");
        cancelledShowRadioButton = new RadioButton("Cancelled");
        statusCheckBox = new CheckBox("Show Approval Buttons");

        allRadioButton.setToggleGroup(group);
        completedlRadioButton.setToggleGroup(group);
        outstandingRadioButton.setToggleGroup(group);
        outstandingRadioButton.setToggleGroup(group);
        rejectedOrderRadioButton.setToggleGroup(group);
        cancelledShowRadioButton.setToggleGroup(group);
        allRadioButton.setSelected(true);

        HBox statusHBox = new HBox(10);
        statusHBox.setPadding(new Insets(5));
        statusHBox.getChildren().add(allRadioButton);
        statusHBox.getChildren().add(completedlRadioButton);
        statusHBox.getChildren().add(outstandingRadioButton);
        statusHBox.getChildren().add(rejectedOrderRadioButton);
        statusHBox.getChildren().add(cancelledShowRadioButton);
        statusHBox.getChildren().add(statusCheckBox);

        initTableView();

        this.setCenter(new VBox(accountHBox, statusHBox, tableView));
    }

    @Override
    public void initTableView() {
        tableView = new TableView();
        clientIdTableColumn = new TableColumn("Client ID");
        statusTableColumn = new TableColumn("Status");
        orderIdTableColumn = new TableColumn("Order ID");
        buySellTableColumn = new TableColumn("Buy/Sell");
        codeTableColumn = new TableColumn("Code");
        priceTableColumn = new TableColumn("Price");
        averageTableColumn = new TableColumn("Average Price");
        quantityTableColumn = new TableColumn("Quantity");
        totalAmountTableColumn = new TableColumn("Total Amount");
        orderTimeTableColumn = new TableColumn("Order Time");
        clientIdTableColumn.setCellValueFactory(new PropertyValueFactory<TradeModel, String>("clientId"));
        buySellTableColumn.setCellValueFactory(new PropertyValueFactory<TradeModel, String>("tradeType"));
        codeTableColumn.setCellValueFactory(new PropertyValueFactory<TradeModel, Double>("stock"));
        priceTableColumn.setCellValueFactory(new PropertyValueFactory<TradeModel, Double>("price"));
        quantityTableColumn.setCellValueFactory(new PropertyValueFactory<TradeModel, Integer>("quantity"));
        orderTimeTableColumn.setCellValueFactory(new PropertyValueFactory<TradeModel, String>("orderTime"));

        tableView.getColumns().add(clientIdTableColumn);
        tableView.getColumns().add(statusTableColumn);
        tableView.getColumns().add(orderIdTableColumn);
        tableView.getColumns().add(buySellTableColumn);
        tableView.getColumns().add(codeTableColumn);
        tableView.getColumns().add(priceTableColumn);
        tableView.getColumns().add(averageTableColumn);
        tableView.getColumns().add(quantityTableColumn);
        tableView.getColumns().add(totalAmountTableColumn);
        tableView.getColumns().add(orderTimeTableColumn);
    }

    @Override
    public Pane getView() {
        return this;
    }

    @Override
    public TextField getAccountTextField() {
        return accountTextField;
    }

    @Override
    public void setAccountTextField(TextField accountTextField) {
        this.accountTextField = accountTextField;
    }

    @Override
    public Label getAccountLabel() {
        return accountLabel;
    }

    @Override
    public void setAccountLabel(Label accountLabel) {
        this.accountLabel = accountLabel;
    }

    @Override
    public CheckBox getAccountCheckBox() {
        return accountCheckBox;
    }

    @Override
    public void setAccountCheckBox(CheckBox accountCheckBox) {
        this.accountCheckBox = accountCheckBox;
    }

    @Override
    public RadioButton getAllRadioButton() {
        return allRadioButton;
    }

    @Override
    public void setAllRadioButton(RadioButton allRadioButton) {
        this.allRadioButton = allRadioButton;
    }

    @Override
    public RadioButton getCompletedlRadioButton() {
        return completedlRadioButton;
    }

    @Override
    public void setCompletedlRadioButton(RadioButton completedlRadioButton) {
        this.completedlRadioButton = completedlRadioButton;
    }

    @Override
    public RadioButton getOutstandingRadioButton() {
        return outstandingRadioButton;
    }

    @Override
    public void setOutstandingRadioButton(RadioButton outstandingRadioButton) {
        this.outstandingRadioButton = outstandingRadioButton;
    }

    @Override
    public RadioButton getRejectedOrderRadioButton() {
        return rejectedOrderRadioButton;
    }

    @Override
    public void setRejectedOrderRadioButton(RadioButton rejectedOrderRadioButton) {
        this.rejectedOrderRadioButton = rejectedOrderRadioButton;
    }

    @Override
    public RadioButton getCancelledShowRadioButton() {
        return cancelledShowRadioButton;
    }

    @Override
    public void setCancelledShowRadioButton(RadioButton cancelledShowRadioButton) {
        this.cancelledShowRadioButton = cancelledShowRadioButton;
    }

    @Override
    public CheckBox getStatusCheckBox() {
        return statusCheckBox;
    }

    @Override
    public void setStatusCheckBox(CheckBox stausCheckBox) {
        this.statusCheckBox = stausCheckBox;
    }

    @Override
    public TableView<TradeModel> getTableView() {
        return tableView;
    }

    @Override
    public void setTableView(TableView<TradeModel> tableView) {
        this.tableView = tableView;
    }

    @Override
    public TableColumn getClientIdTableColumn() {
        return clientIdTableColumn;
    }

    @Override
    public void setClientIdTableColumn(TableColumn clientIdTableColumn) {
        this.clientIdTableColumn = clientIdTableColumn;
    }

    @Override
    public TableColumn getStatusTableColumn() {
        return statusTableColumn;
    }

    @Override
    public void setStatusTableColumn(TableColumn statusTableColumn) {
        this.statusTableColumn = statusTableColumn;
    }

    @Override
    public TableColumn getOrderIdTableColumn() {
        return orderIdTableColumn;
    }

    @Override
    public void setOrderIdTableColumn(TableColumn orderIdTableColumn) {
        this.orderIdTableColumn = orderIdTableColumn;
    }

    @Override
    public TableColumn getBuySellTableColumn() {
        return buySellTableColumn;
    }

    @Override
    public void setBuySellTableColumn(TableColumn buySellTableColumn) {
        this.buySellTableColumn = buySellTableColumn;
    }

    @Override
    public TableColumn getCodeTableColumn() {
        return codeTableColumn;
    }

    @Override
    public void setCodeTableColumn(TableColumn codeTableColumn) {
        this.codeTableColumn = codeTableColumn;
    }

    @Override
    public TableColumn getPriceTableColumn() {
        return priceTableColumn;
    }

    @Override
    public void setPriceTableColumn(TableColumn priceTableColumn) {
        this.priceTableColumn = priceTableColumn;
    }

    @Override
    public TableColumn getAverageTableColumn() {
        return averageTableColumn;
    }

    @Override
    public void setAverageTableColumn(TableColumn averageTableColumn) {
        this.averageTableColumn = averageTableColumn;
    }

    @Override
    public TableColumn getQuantityTableColumn() {
        return quantityTableColumn;
    }

    @Override
    public void setQuantityTableColumn(TableColumn quantityTableColumn) {
        this.quantityTableColumn = quantityTableColumn;
    }

    @Override
    public TableColumn getTotalAmountTableColumn() {
        return totalAmountTableColumn;
    }

    @Override
    public void setTotalAmountTableColumn(TableColumn totalAmountTableColumn) {
        this.totalAmountTableColumn = totalAmountTableColumn;
    }

    @Override
    public TableColumn getOrderTimeTableColumn() {
        return orderTimeTableColumn;
    }

    @Override
    public void setOrderTimeTableColumn(TableColumn orderTimeTableColumn) {
        this.orderTimeTableColumn = orderTimeTableColumn;
    }
}
