package com.hee.pea.training.view;

import javafx.scene.layout.BorderPane;

import java.text.DecimalFormat;
import java.text.NumberFormat;

public abstract class AbstractView extends BorderPane {

	public void isFocusView(boolean isFocus) {
		if (isFocus) {
			this.getStyleClass().remove("grey-border");
			this.getStyleClass().add("lime-border");
		} else {
			this.getStyleClass().remove("lime-border");
			this.getStyleClass().add("grey-border");
		}
	}
}
