package com.hee.pea.training.view;

import javafx.scene.control.*;

public interface LoginView extends BaseView {

    Label getLoginIdLabel();

    Label getLoginPwLabel();

    Label getLanguageLabel();

    TextField getLoginIdTextField();

    PasswordField getLoginPwtextField();

    ComboBox getLanguageComboBox();

    CheckBox getSaveIdCheckBox();

    CheckBox getSavePwCheckBox();

    Button getLoginButton();

    Hyperlink getForgetPwButton();

}
