package com.hee.pea.training.view;

import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;

public interface MenuBarView extends BaseView {

    MenuBar getMenuBar();

    Menu getSystemMenu();

    MenuItem getMenuItem();
}
