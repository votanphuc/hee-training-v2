package com.hee.pea.training.controller;

import com.hee.pea.training.presenter.AbstractPresenter;
import javafx.stage.Stage;

public interface StageController {

    void start(final Stage primaryStage, AbstractPresenter presenter);

    Stage getPrimaryStage();

    void loadScreen(final AbstractPresenter presenter);
}
