package com.hee.pea.training.controller;

import com.hee.pea.training.presenter.AbstractPresenter;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class StageControllerImpl implements StageController { //TODO:

    private static final Logger LOGGER = LogManager.getLogger(StageControllerImpl.class);

    private Stage primaryStage;

    private Stage stage;

//    private ConcurrentHashMap<StageStyle, Stage> stages = new ConcurrentHashMap<StageStyle, Stage>();

    public StageControllerImpl(){
        this.stage = new Stage();
        this.primaryStage = new Stage();

    }
    @Override
    public void start(Stage primaryStage, AbstractPresenter presenter) {
        if (LOGGER.isDebugEnabled()) LOGGER.debug("start...");

        this.primaryStage = primaryStage;
//        this.stage = selectStage(loginPresenter);
        this.stage.initOwner(this.primaryStage);
        this.stage.resizableProperty().setValue(Boolean.FALSE);
        this.stage.setTitle("Hee Training...");
        loadScreen(presenter);

    }

    @Override
    public void loadScreen(AbstractPresenter presenter) {

        this.stage.setScene(new Scene(presenter.getParent()));
        this.stage.hide();
        this.stage.show();
        this.stage.sizeToScene();
        this.stage.centerOnScreen();
    }

    @Override
    public Stage getPrimaryStage() {
        return primaryStage;
    }

}
