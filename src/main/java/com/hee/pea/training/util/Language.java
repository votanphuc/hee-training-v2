package com.hee.pea.training.util;

import com.hee.pea.training.constant.ResourceType;
import javafx.beans.binding.StringBinding;
import javafx.beans.property.ObjectProperty;

import java.util.Locale;
import java.util.Map;

public interface Language {

    Locale getLocale();

    void setLocale(final Locale locale);

    void setLocale(final String language);

    ObjectProperty<Locale> localeProperty();

    String getLanguage();

    Map<Locale, String> getLanguageKeyMap();

    Map<String, Locale> getLanguageValMap();

    StringBinding labelBinding(final String key);

    StringBinding resultBinding(final String key);

    StringBinding binding(final ResourceType type, final String key);

}
