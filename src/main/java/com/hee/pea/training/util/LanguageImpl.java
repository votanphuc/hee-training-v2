package com.hee.pea.training.util;

import com.google.common.base.Strings;
import com.hee.pea.training.constant.ResourceType;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.StringBinding;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableMap;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.annotation.Resource;
import java.text.MessageFormat;
import java.util.*;

public class LanguageImpl implements Language {//TODO
    private static final Logger LOGGER = LogManager.getLogger(LanguageImpl.class);

    public static final Locale LANG_HK = new Locale("zh", "HK");

    public static final Locale LANG_CN = new Locale("zh", "CN");

    public static final Locale LANG_EN = new Locale("en");

    private final ObjectProperty<Locale> locale;

    private final ObservableMap<Locale, String> langMap;

    private final Map<Locale, String> keyMap = new LinkedHashMap<>();

    private final Map<String, Locale> valMap = new LinkedHashMap<>();

    private final List<Locale> langList = new ArrayList<>();

    private String savedLanguage;

//    @Resource(name = "preference")
//    private Preference preference;

    public LanguageImpl() {
        if (LOGGER.isDebugEnabled()) LOGGER.debug("Language...");
        locale = new SimpleObjectProperty<>(LANG_EN);

        keyMap.put(LANG_EN, getStringFromBundle(ResourceType.LABEL, "login_language.EN"));
        keyMap.put(LANG_HK, getStringFromBundle(ResourceType.LABEL, "login_language.HK"));
        keyMap.put(LANG_CN, getStringFromBundle(ResourceType.LABEL, "login_language.CN"));
        valMap.put(getStringFromBundle(ResourceType.LABEL, "login_language.EN"), LANG_EN);

        valMap.put(getStringFromBundle(ResourceType.LABEL, "login_language.HK"), LANG_HK);
        valMap.put(getStringFromBundle(ResourceType.LABEL, "login_language.CN"), LANG_CN);
        langList.add(LANG_HK);
        langList.add(LANG_CN);
        langList.add(LANG_EN);
        langMap = FXCollections.observableMap(new LinkedHashMap<Locale, String>(getLanguageKeyMap()));
    }

    @Override
    public Locale getLocale() {
        return locale.get();
    }

    @Override
    public void setLocale(Locale locale) {
        localeProperty().set(locale);
        Locale.setDefault(locale);
//        preference.putString(true, PrefKey.LANGUAGE, locale.toString());
    }

    @Override
    public void setLocale(String language) {
        Locale locale = getLanguageValMap().get(language);
        setLocale(locale);
    }

    @Override
    public ObjectProperty<Locale> localeProperty() {
        return locale;
    }

    @Override
    public String getLanguage() {
        return langMap.get(locale.get());
    }

    @Override
    public Map<Locale, String> getLanguageKeyMap() {
        return keyMap;
    }

    @Override
    public Map<String, Locale> getLanguageValMap() {
        return valMap;
    }

    @Override
    public StringBinding labelBinding(String key) {
        return createStringBinding(ResourceType.LABEL, key);
    }

    @Override
    public StringBinding resultBinding(String key) {
        return createStringBinding(ResourceType.RESULT, key);
    }

    @Override
    public StringBinding binding(ResourceType type, String key) {
        return createStringBinding(type, key);
    }


    private ResourceBundle getResourceBundle(final String type) {
        return ResourceBundle.getBundle(type, getLocale());
    }

    private String getStringFromBundle(final ResourceType type, final String key) {
        try {
            return getResourceBundle(type.getValue()).getString(key);
        } catch (MissingResourceException | NullPointerException e) {
            return key;
        }
    }

    private StringBinding createStringBinding(final ResourceType type, final String key, Object... args) {
        return Bindings.createStringBinding(() -> prepareString(type, key, null, null, args), locale);
    }

    private String prepareString(final ResourceType type, final String key, final String before, final String after, final Object... args) {
        return MessageFormat.format(nullToEmpty(before) + getStringFromBundle(type, key) + nullToEmpty(after), args);
    }

    private String nullToEmpty(final String string) {
        return Strings.isNullOrEmpty(string) ? "" : string;
    }

}
