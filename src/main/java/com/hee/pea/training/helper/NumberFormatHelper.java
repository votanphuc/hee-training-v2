package com.hee.pea.training.helper;

import com.google.common.base.Strings;
import javafx.util.StringConverter;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.math.BigDecimal;
import java.text.DecimalFormat;

public class NumberFormatHelper {

    // constant
    private static final String DP_0 = "#,##0";
    private static final String DP_1 = "#,##0.0";
    private static final String DP_2 = "#,##0.00";
    private static final String DP_3 = "#,##0.000";
    private static final String DP_4 = "#,##0.0000";
    private static final Long ONE_B = 1_000_000_000L;
    private static final Long ONE_M = 1_000_000L;
    private static final Long ONE_K = 1_000L;
	// logger
    private static Logger LOGGER = LogManager.getLogger(NumberFormatHelper.class.getName());

    // amount
    public static String formatAmount(BigDecimal amount) {
        if (amount == null) {
            return "-";
        }

        return new DecimalFormat(DP_2).format(amount);
    }
    
    public static String formatAmountDP3(BigDecimal amount) {
        if (amount == null) {
            return "-";
        }
        return new DecimalFormat(DP_3).format(amount);
    }

    // changes
    public static String formatChange(BigDecimal netChange, BigDecimal percentChange) {
        return formatPrice(netChange) + " (" + formatPercentChange(percentChange) + ")";
    }

    // market depth
    public static String formatMarketDepth(Long quantity) {
    	String quantityString;
    	
    	if (quantity == null) {
			quantityString = "";
		} else if (ONE_B.compareTo(quantity) <= 0) {
			quantityString = new DecimalFormat(DP_0).format(quantity / ONE_B) + "B";
		} else if (ONE_M.compareTo(quantity) <= 0) {
			quantityString = new DecimalFormat(DP_0).format(quantity / ONE_M) + "M";
		} else if (ONE_K.compareTo(quantity) <= 0) {
			quantityString = new DecimalFormat(DP_0).format(quantity / ONE_K) + "K";
		} else {
			quantityString = new DecimalFormat(DP_0).format(quantity);
		}
    	
    	return quantityString;
    }

    // percent change
    public static String formatPercentChange(BigDecimal percentChange) {
        if (percentChange == null) {
            return "-";
        }

        if (BigDecimal.ZERO.compareTo(percentChange) < 0) {
            return "+" + new DecimalFormat(DP_2).format(percentChange) + "%";
        } else {
            return new DecimalFormat(DP_2).format(percentChange) + "%";
        }
    }
    
    public static String formatPercentChange(Integer percentChange) {
    	if (percentChange == null) {
            return "-";
        }

        if (percentChange.compareTo(0) < 0) {
            return "+" + new DecimalFormat(DP_2).format(percentChange) + "%";
        } else {
            return new DecimalFormat(DP_2).format(percentChange) + "%";
        }
    }

    // price
    public static String formatPrice(BigDecimal price) {
        if (price == null) {
            return "-";
        }

        switch (price.scale()) {
            case 0:
                return new DecimalFormat(DP_0).format(price);
            case 1:
                return new DecimalFormat(DP_1).format(price);
            case 2:
                return new DecimalFormat(DP_2).format(price);
            case 3:
                return new DecimalFormat(DP_3).format(price);
            default:
                return new DecimalFormat(DP_4).format(price);
        }
    }

    // quantity
    public static String formatQuantity(BigDecimal quantity) {
        if (quantity == null) {
            return "-";
        }

        return new DecimalFormat(DP_0).format(quantity);
    }
    
    // quantity
    public static String formatQuantity(long quantity) {

        return new DecimalFormat(DP_0).format(quantity);
    }

    // rate
    public static String formatRate(BigDecimal rate) {
        if (rate == null) {
            return "-";
        }

        return new DecimalFormat(DP_4).format(rate);
    }
    
    // count
    public static String formatCount(Integer count) {
    	String countString;
    	
    	if (count == null) {
    		countString = new DecimalFormat(DP_0).format(0);
		} else {
			countString = new DecimalFormat(DP_0).format(count);
		}
    	
    	if(countString.length() == 1) {
    		return "(      " + countString + ")";
    	} else if(countString.length() == 2) {
    		return "(    " + countString + ")";
    	} else if(countString.length() == 3) {
    		return "(  " + countString + ")";
    	} else {
    		return "(" + countString + ")";
    	}
    }
    
    public static StringConverter<Integer> integerConverter() {
    	return new StringConverter<Integer>() {

			@Override
			public String toString(Integer object) {
				if (object == null) {
	                return null;
	            }
				return object.toString();
			}

			@Override
			public Integer fromString(String string) {
				if (Strings.isNullOrEmpty(string) || string.equals("-")) {
					return null;
				}
				return Integer.parseInt(string);
			}
		};
    }
    
    public static StringConverter<Long> longConverter() {
		return new StringConverter<Long>() {

			@Override
			public String toString(Long object) {
				if (object == null) {
	                return "";
	            }
				return NumberFormatHelper.formatQuantity(object);
			}

			@Override
			public Long fromString(String string) {
				if (Strings.isNullOrEmpty(string) || string.equals("-")) {
					return null;
				}
				return new Long(StringUtils.replace(string,",", ""));
			}
		};
    }
    
    public static StringConverter<Double> doubleConverter() {
    	return new StringConverter<Double>() {

			@Override
			public String toString(Double object) {
				if (object == null) {
	                return null;
	            }
				return object.toString();
			}

			@Override
			public Double fromString(String string) {
				if (Strings.isNullOrEmpty(string)) {
					return null;
				}
				return Double.parseDouble(string);
			}
    	};
    }
    
    public static StringConverter<BigDecimal> quantityConverter() {
		return new StringConverter<BigDecimal>() {

			@Override
			public String toString(BigDecimal object) {
				if (object == null) {
	                return "-";
	            }
				return NumberFormatHelper.formatQuantity(object);
			}

			@Override
			public BigDecimal fromString(String string) {
				if (Strings.isNullOrEmpty(string) || string.equals("-")) {
					return BigDecimal.ZERO;
				}
				return new BigDecimal(StringUtils.replace(string,",", ""));
			}
		};
    }
    
    public static StringConverter<BigDecimal> quantityWithNullConverter() {
		return new StringConverter<BigDecimal>() {

			@Override
			public String toString(BigDecimal object) {
				if (object == null) {
	                return "";
	            }
				return NumberFormatHelper.formatQuantity(object);
			}

			@Override
			public BigDecimal fromString(String string) {
				if (Strings.isNullOrEmpty(string) || string.equals("-")) {
					return null;
				}
				return new BigDecimal(StringUtils.replace(string,",", ""));
			}
		};
    }
    
    public static StringConverter<BigDecimal> currencyConverter() {
    	return new StringConverter<BigDecimal>() {

			@Override
			public String toString(BigDecimal object) {
				if (object == null) {
	                return "-";
	            }
				return NumberFormatHelper.formatAmountDP3(object);
			}

			@Override
			public BigDecimal fromString(String string) {
				if (Strings.isNullOrEmpty(string) || string.equals("-")) {
					return BigDecimal.ZERO;
				}
				return new BigDecimal(StringUtils.replace(string,",", ""));
			}
		};
    }
    
    public static StringConverter<BigDecimal> currencyWIthNullConverter() {
    	return new StringConverter<BigDecimal>() {

			@Override
			public String toString(BigDecimal object) {
				if (object == null) {
	                return "";
	            }
				return NumberFormatHelper.formatAmountDP3(object);
			}

			@Override
			public BigDecimal fromString(String string) {
				if (Strings.isNullOrEmpty(string) || string.equals("-")) {
					return null;
				}
				return new BigDecimal(StringUtils.replace(string,",", ""));
			}
		};
    }
    
    // 2dp
    public static String formatbd2dp(BigDecimal value) {
        if (value == null) {
            return "";
        }

        return new DecimalFormat(DP_2).format(value);
    }
    
    // 3dp
    public static String formatbd3dp(BigDecimal value) {
        if (value == null) {
            return "";
        }

        return new DecimalFormat(DP_3).format(value);
    }
    
    // 4dp
    public static String formatbd4dp(BigDecimal value) {
        if (value == null) {
            return "";
        }

        return new DecimalFormat(DP_4).format(value);
    }
}
