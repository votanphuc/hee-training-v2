package com.hee.pea.training.viewmodel;

public abstract class AbstractViewModel {

    protected String viewId;

    public String getViewId() {
        return viewId;
    }

    public void setViewId(String viewId) {
        this.viewId = viewId;
    }
}
