package com.hee.pea.training.viewmodel;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class LoginViewModel extends AbstractViewModel {

    private final StringProperty loginId;

    private final StringProperty loginPw;

    private final ObjectProperty<Boolean> saveId;

    private final ObjectProperty<Boolean> savePw;

    public LoginViewModel() {
        loginId = new SimpleStringProperty();
        loginPw = new SimpleStringProperty();
        saveId = new SimpleObjectProperty<>();
        savePw = new SimpleObjectProperty<>();
    }

    public final StringProperty loginIdProperty() {
        return this.loginId;
    }

    public final String getLoginId() {
        return this.loginIdProperty().get();
    }

    public final void setLoginId(final String loginId) {
        this.loginIdProperty().set(loginId);
    }

    public final StringProperty loginPwProperty() {
        return this.loginPw;
    }

    public final String getLoginPw() {
        return this.loginPwProperty().get();
    }

    public final void setLoginPw(final String loginPw) {
        this.loginPwProperty().set(loginPw);
    }

    public final ObjectProperty<Boolean> saveIdProperty() {
        return this.saveId;
    }

    public final Boolean getSaveId() {
        return this.saveIdProperty().get();
    }

    public final void setSaveId(final Boolean saveId) {
        this.saveIdProperty().set(saveId);
    }

    public final ObjectProperty<Boolean> savePwProperty() {
        return this.savePw;
    }

    public final Boolean getSavePw() {
        return this.savePwProperty().get();
    }

    public final void setSavePw(final Boolean savePw) {
        this.savePwProperty().set(savePw);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("LoginViewModel [loginId=");
        builder.append(loginId);
        builder.append(", loginPw=");
        builder.append(loginPw);
        builder.append(", saveId=");
        builder.append(saveId);
        builder.append(", savePw=");
        builder.append(savePw);
        builder.append(", viewId=");
        builder.append(viewId);
        builder.append("]");
        return builder.toString();
    }
}
