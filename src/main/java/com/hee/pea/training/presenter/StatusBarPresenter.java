package com.hee.pea.training.presenter;

import com.hee.pea.training.view.StatusBarView;
import com.hee.pea.training.viewmodel.StatusBarViewModel;

public class StatusBarPresenter extends AbstractPresenter<StatusBarView, StatusBarViewModel> {

    public StatusBarPresenter(StatusBarView view, StatusBarViewModel viewModel) {
        super(view, viewModel);
    }

    @Override
    public void bindLabel() {

    }

    @Override
    public void bind() {

    }
}
