package com.hee.pea.training.presenter;

import com.google.common.base.Strings;
import com.hee.pea.training.controller.StageController;
import com.hee.pea.training.controller.StageControllerImpl;
import com.hee.pea.training.model.TradeModel;
import com.hee.pea.training.service.OrderTicketService;
import com.hee.pea.training.service.TradeService;
import com.hee.pea.training.service.impl.OrderTicketServiceImpl;
import com.hee.pea.training.service.impl.TradeServiceImpl;
import com.hee.pea.training.util.Language;
import com.hee.pea.training.util.LanguageImpl;
import com.hee.pea.training.view.OrderTicketView;
import com.hee.pea.training.view.TradeView;
import com.hee.pea.training.viewmodel.TradeViewModel;
import javafx.event.ActionEvent;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class TradePresenter extends AbstractPresenter<TradeView, TradeViewModel> {

    private static Logger LOGGER = LogManager.getLogger(TradePresenter.class);

    private TradeService tradeService = new TradeServiceImpl();

    private OrderTicketService orderTicketService = new OrderTicketServiceImpl();

    private StageController stageController = new StageControllerImpl();

    private Language language = new LanguageImpl();

    public TradePresenter(TradeView view, TradeViewModel viewModel) {
        super(view, viewModel);
    }

    @Override
    public void bindLabel() {
    }

    @Override
    public void bind() {
        LOGGER.info("Binding trade presenter");
        view.getClearButton().setOnAction(this::handleOnClearClick);
        view.getAccountCheckBox().setOnAction(this::handleAccountCheckBox);
        view.getStockCheckBox().setOnAction(this::handleStockCheckBox);
        view.getTypeCheckBox().setOnAction(this::handleTypeCheckBox);
        view.getbPxCheckBox().setOnAction(this::handlePBxCheckBox);
        view.getQtyCheckBox().setOnAction(this::handleQtyCheckBox);

        view.getbPxPlusButton().setOnAction(actionEvent -> handleOnPlusClick(actionEvent, view.getbPxTextField()));
        view.getbPxMinusButton().setOnAction(actionEvent -> handleOnMinusClick(actionEvent, view.getbPxTextField()));
        view.getQtyPlusButton().setOnAction(actionEvent -> handleOnPlusClick(actionEvent, view.getQtyTextField()));
        view.getQtyMinusButton().setOnAction(actionEvent -> handleOnMinusClick(actionEvent, view.getQtyTextField()));
    }

    public void handleOnClearClick(ActionEvent actionEvent) {
        LOGGER.info("Clear button clicked");
        view.getAccountTextField().clear();
        view.getStockTextField().clear();
        view.getbPxTextField().clear();
        view.getQtyTextField().clear();
        view.getTypeComboBox().getSelectionModel().selectFirst();
    }

    public void handleOnBuyClick(ActionEvent actionEvent, OrderTicketView orderTicketView, TradeModel tradeModel) {
        LOGGER.info("Buy button clicked");
        orderTicketService.addRecord(orderTicketView, tradeModel);
    }

    public void handleOnPlusClick(ActionEvent actionEvent, TextField textField) {
        LOGGER.info("Plus TextField {}", textField.getId());
        if (!Strings.isNullOrEmpty(textField.getText())) {
            Double val = 0d;
            try {
                val = Double.parseDouble(textField.getText());
            } catch (Exception e) {
                LOGGER.error("NaN, {}", e);
            }
            textField.setText(String.valueOf(++val));
        }
    }

    public void handleOnMinusClick(ActionEvent actionEvent, TextField textField) {
        LOGGER.info("Minus TextField {}", textField.getId());
        if (!Strings.isNullOrEmpty(textField.getText())) {
            Double val = 0d;
            try {
                val = Double.parseDouble(textField.getText());
            } catch (Exception e) {
                LOGGER.error("NaN, {}", e);
            }
            textField.setText(String.valueOf(--val));
        }
    }

    public void handleAccountCheckBox(ActionEvent actionEvent) {
        if (!view.getAccountCheckBox().isSelected()) {
            view.getAccountTextField().setDisable(false);
        } else {
            view.getAccountTextField().setDisable(true);
        }
    }

    public void handleStockCheckBox(ActionEvent actionEvent) {
        if (!view.getStockCheckBox().isSelected()) {
            view.getStockTextField().setDisable(false);
        } else {
            view.getStockTextField().setDisable(true);
        }
    }

    public void handleTypeCheckBox(ActionEvent actionEvent) {
        if (!view.getTypeCheckBox().isSelected()) {
            view.getTypeComboBox().setDisable(false);
        } else {
            view.getTypeComboBox().setDisable(true);
        }
    }

    public void handlePBxCheckBox(ActionEvent actionEvent) {
        if (!view.getbPxCheckBox().isSelected()) {
            view.getbPxTextField().setDisable(false);
            view.getbPxPlusButton().setDisable(false);
            view.getbPxMinusButton().setDisable(false);
        } else {
            view.getbPxTextField().setDisable(true);
            view.getbPxPlusButton().setDisable(true);
            view.getbPxMinusButton().setDisable(true);
        }
    }

    public void handleQtyCheckBox(ActionEvent actionEvent) {
        if (!view.getQtyCheckBox().isSelected()) {
            view.getQtyTextField().setDisable(false);
            view.getQtyPlusButton().setDisable(false);
            view.getQtyMinusButton().setDisable(false);
        } else {
            view.getQtyTextField().setDisable(true);
            view.getQtyPlusButton().setDisable(true);
            view.getQtyMinusButton().setDisable(true);
        }
    }

    public void handleEnterAccountTextField(String text, TextField accountTextField) {
        bindingClientId(text, accountTextField);
        view.getStockTextField().requestFocus();
    }

    public void bindingClientId(String clientId, TextField textField) {
        LOGGER.info("Binding ClientId {}", clientId);
        textField.setText(clientId);

    }

    public void handleEnterStockTextField() {
        view.getTypeComboBox().requestFocus();
    }

    public void handleEnterTypeComboBox() {
        view.getbPxTextField().requestFocus();
    }

    public void handleEnterBPxTextField() {
        view.getQtyTextField().requestFocus();
    }

    public void handleEnterQtyTextField(KeyEvent event, OrderTicketView orderTicketView, TradeModel tradeData) {
        orderTicketService.addRecord(orderTicketView, tradeData);
        view.getBuyButton().requestFocus();
    }
}

