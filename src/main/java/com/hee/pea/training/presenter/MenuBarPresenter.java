package com.hee.pea.training.presenter;

import com.hee.pea.training.service.MenuBarService;
import com.hee.pea.training.service.impl.MenuBarServiceImpl;
import com.hee.pea.training.view.MenuBarView;
import com.hee.pea.training.viewmodel.MenuBarViewModel;
import javafx.event.ActionEvent;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class MenuBarPresenter extends AbstractPresenter<MenuBarView, MenuBarViewModel> {

    private static Logger LOGGER = LogManager.getLogger(MenuBarPresenter.class);

    private MenuBarService menuBarService = new MenuBarServiceImpl();

    public MenuBarPresenter(MenuBarView view, MenuBarViewModel viewModel) {
        super(view, viewModel);
    }

    @Override
    public void bindLabel() {

    }

    @Override
    public void bind() {
        this.view.getMenuItem().setOnAction(this::handleExitEvent);
    }

    public void handleExitEvent(ActionEvent actionEvent) {
        LOGGER.info("Exit");
        menuBarService.exit();
    }
}
