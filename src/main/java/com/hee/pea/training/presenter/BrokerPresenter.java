package com.hee.pea.training.presenter;

import com.hee.pea.training.model.BrokerIdModel;
import com.hee.pea.training.model.BrokerNameModel;
import com.hee.pea.training.service.BrokerService;
import com.hee.pea.training.service.impl.BrokerServiceImpl;
import com.hee.pea.training.view.BrokerView;
import com.hee.pea.training.viewmodel.BrokerViewModel;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class BrokerPresenter extends AbstractPresenter<BrokerView, BrokerViewModel> {

    private static Logger LOGGER = LogManager.getLogger(BrokerPresenter.class);

    private BrokerService brokerService = new BrokerServiceImpl();

    public BrokerPresenter(BrokerView view, BrokerViewModel viewModel) {
        super(view, viewModel);
    }

    @Override
    public void bindLabel() {

    }

    @Override
    public void bind() {

    }

    @Override
    public void initialization() {
        LOGGER.info("Add fake data for BrokerTable");

        BrokerView view = (BrokerView) this.view.getView();

        ObservableList<BrokerIdModel> dataBrokerId =
                FXCollections.observableArrayList(
                        new BrokerIdModel(12.0,13.44,232.3),
                        new BrokerIdModel(242.0,124.44,262.35),
                        new BrokerIdModel(12.69,113.24,242.13)
                );
        view.getBrokerIdTableView().setItems(dataBrokerId);
        ObservableList<BrokerNameModel> dataBrokerName =
                FXCollections.observableArrayList(
                        new BrokerNameModel(12.0,123.44),
                        new BrokerNameModel(242.0,124.44),
                        new BrokerNameModel(12.69,113.24)
                );
        view.getBrokerNameTableView().setItems(dataBrokerName);

    }
}
