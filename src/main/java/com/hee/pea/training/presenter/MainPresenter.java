package com.hee.pea.training.presenter;

import com.hee.pea.training.model.TradeModel;
import com.hee.pea.training.view.MainView;
import com.hee.pea.training.view.TradeView;
import com.hee.pea.training.viewmodel.MainViewModel;
import javafx.scene.input.KeyCode;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class MainPresenter extends AbstractPresenter<MainView, MainViewModel> {

    private static Logger LOGGER = LogManager.getLogger(MainPresenter.class);

    public MainPresenter(MainView view, MainViewModel viewModel) {
        super(view, viewModel);
    }

    @Override
    public void bindLabel() {
    }

    @Override
    public void bind() {
        LOGGER.info("Handle onclick");

        TradeView tradeView = this.view.getTradeView();
        TradePresenter tradePresenter = this.view.getTradePresenter();

        tradeView.getAccountTextField().setOnKeyReleased((event) -> {
            if (event.getCode() == KeyCode.ENTER) {
                tradePresenter.handleEnterAccountTextField(tradeView.getAccountTextField().getText(), this.view.getOrderTicketView().getAccountTextField());
            }
        });
        tradeView.getStockTextField().setOnKeyReleased((event) -> {
            if (event.getCode() == KeyCode.ENTER) {
                tradePresenter.handleEnterStockTextField();
            }
        });
        tradeView.getTypeComboBox().setOnKeyReleased((event) -> {
            if (event.getCode() == KeyCode.ENTER) {
                tradePresenter.handleEnterTypeComboBox();
            }
        });
        tradeView.getbPxTextField().setOnKeyReleased((event) -> {
            if (event.getCode() == KeyCode.ENTER) {
                tradePresenter.handleEnterBPxTextField();
            }
        });
        tradeView.getQtyTextField().setOnKeyReleased((event) -> {
            if (event.getCode() == KeyCode.ENTER) {
                tradePresenter.handleEnterQtyTextField(event, this.view.getOrderTicketView(), getTradeData());
            }
        });

        tradeView.getBuyButton().setOnAction((actionEvent ->
                tradePresenter.handleOnBuyClick(actionEvent, this.view.getOrderTicketView(), getTradeData())));
    }

    private TradeModel getTradeData() {
        String clientId = this.view.getTradeView().getAccountTextField().getText();
        Double stock = Double.parseDouble((this.view.getTradeView().getStockTextField().getText()));
        Double price = Double.parseDouble((this.view.getTradeView().getbPxTextField().getText()));

//        Double stock = (Double) ((NumberTextField)this.view.getTradeView().getStockTextField()).getValue();
//        Double price = (Double) ((NumberTextField)this.view.getTradeView().getbPxTextField()).getValue();
        Integer quantity = Integer.parseInt((this.view.getTradeView().getQtyTextField().getText()));

        return new TradeModel(clientId, stock, price, quantity);
    }

    @Override
    public void initialization() {
        this.view.getBrokerPresenter().initialization();
    }

}
