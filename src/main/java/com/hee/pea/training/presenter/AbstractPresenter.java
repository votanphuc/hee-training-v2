package com.hee.pea.training.presenter;

import com.hee.pea.training.view.BaseView;
import com.hee.pea.training.viewmodel.AbstractViewModel;
import javafx.scene.layout.Pane;

public abstract class AbstractPresenter<V extends BaseView, M extends AbstractViewModel> {

    protected V view;

    protected M viewModel;

    public AbstractPresenter(V view, M viewModel) {
        this.view = view;
        this.viewModel = viewModel;
        bindLabel();
        bind();
        initialization();
    }

    public abstract void bindLabel();

    public abstract void bind();

    public void initialization() {
    }

    public Pane getParent() {
        return view.getView();
    }

    public V getView() {
        return view;
    }

}
