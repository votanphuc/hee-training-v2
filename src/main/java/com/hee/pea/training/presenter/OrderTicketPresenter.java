package com.hee.pea.training.presenter;

import com.hee.pea.training.view.OrderTicketView;
import com.hee.pea.training.viewmodel.OrderTicketViewModel;

public class OrderTicketPresenter extends AbstractPresenter<OrderTicketView, OrderTicketViewModel> {

    public OrderTicketPresenter(OrderTicketView view, OrderTicketViewModel viewModel) {
        super(view, viewModel);
    }

    @Override
    public void bindLabel() {

    }

    @Override
    public void bind() {

    }
}
