package com.hee.pea.training.presenter;

import com.google.common.base.Strings;
import com.hee.pea.training.controller.StageController;
import com.hee.pea.training.controller.StageControllerImpl;
import com.hee.pea.training.helper.AlertHelper;
import com.hee.pea.training.service.LoginService;
import com.hee.pea.training.service.impl.LoginServiceImpl;
import com.hee.pea.training.util.Language;
import com.hee.pea.training.util.LanguageImpl;
import com.hee.pea.training.view.LoginView;
import com.hee.pea.training.view.impl.MainViewImpl;
import com.hee.pea.training.viewmodel.LoginViewModel;
import com.hee.pea.training.viewmodel.MainViewModel;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.scene.control.Alert;
import javafx.stage.Stage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class LoginPresenter extends AbstractPresenter<LoginView, LoginViewModel> {

    private static final Logger LOGGER = LogManager.getLogger(LoginPresenter.class);

    private LoginService loginService = new LoginServiceImpl();

    private StageController stageController = new StageControllerImpl();

    private static Language language = new LanguageImpl();

    public LoginPresenter(LoginView view, LoginViewModel viewModel) {
        super(view, viewModel);
    }

    @Override
    public void bindLabel() {
        this.view.getLoginIdLabel().textProperty().bind(language.labelBinding("login_client_id_id"));
        this.view.getLoginPwLabel().textProperty().bind(language.labelBinding("login_password"));
        this.view.getLanguageLabel().textProperty().bind(language.labelBinding("setting_page_language"));
        this.view.getSaveIdCheckBox().textProperty().bind(language.labelBinding("login_remember_login_id"));
        this.view.getSavePwCheckBox().textProperty().bind(language.labelBinding("login_remember_login_password"));
        this.view.getLoginButton().textProperty().bind(language.labelBinding("login"));
        this.view.getForgetPwButton().textProperty().bind(language.labelBinding("login_forget_password"));
    }

    @Override
    public void bind() {
        this.view.getLoginIdTextField().textProperty().bindBidirectional(this.viewModel.loginIdProperty());
        this.view.getLoginPwtextField().textProperty().bindBidirectional(this.viewModel.loginPwProperty());
        this.view.getSaveIdCheckBox().selectedProperty().bindBidirectional(this.viewModel.saveIdProperty());
        this.view.getSavePwCheckBox().selectedProperty().bindBidirectional(this.viewModel.savePwProperty());

        this.view.getLoginButton().setOnAction(this::handleOnLoginClick);
        this.view.getLanguageComboBox().getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (oldValue != null && oldValue != newValue) {
                language.setLocale(newValue.toString());
            }
        });
    }

    @SuppressWarnings("unchecked")
    @Override
    public void initialization() {
        super.initialization();

        setLanguageComboBox();
    }

    private void setLanguageComboBox() {
//        final int index = this.view.getLanguageComboBox().getSelectionModel().getSelectedIndex();
        this.view.getLanguageComboBox().setItems(null);
        this.view.getLanguageComboBox().setItems(FXCollections.observableArrayList(language.getLanguageKeyMap().values()));
        this.view.getLanguageComboBox().getSelectionModel().selectFirst();
    }

    private void handleOnLoginClick(final ActionEvent actionEvent) {
        LOGGER.info("Login button clicked");

        String loginId = viewModel.getLoginId();
        String loginPw = viewModel.getLoginPw();

        if (Strings.isNullOrEmpty(loginId) || Strings.isNullOrEmpty(loginPw)) {
            LOGGER.info("Input empty");
            AlertHelper.showAlert(Alert.AlertType.ERROR, "Input Error!", "Please enter your username/password");
            return;
        }

        Boolean loginResult = loginService.login(loginId, loginPw);

        if (loginResult) {
            ((Stage) this.getParent().getScene().getWindow()).close();
            MainPresenter mainPresenter = new MainPresenter(new MainViewImpl(), new MainViewModel());
            stageController.loadScreen(mainPresenter);
            ((Stage) mainPresenter.getParent().getScene().getWindow()).setMaximized(true);
            ((Stage) mainPresenter.getParent().getScene().getWindow()).setResizable(true);
            ((Stage) mainPresenter.getParent().getScene().getWindow()).setTitle("Hee Training - Trading");

        } else {
            LOGGER.info("Login failed!");
            AlertHelper.showAlert(Alert.AlertType.ERROR, "Login Error!", "Invalid username/password");
        }
    }

}
