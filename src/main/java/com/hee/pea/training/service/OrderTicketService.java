package com.hee.pea.training.service;

import com.hee.pea.training.model.TradeModel;
import com.hee.pea.training.view.OrderTicketView;

public interface OrderTicketService {

    void addRecord(OrderTicketView view, TradeModel tradeModel);
}
