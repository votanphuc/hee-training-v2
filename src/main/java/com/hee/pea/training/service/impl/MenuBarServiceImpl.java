package com.hee.pea.training.service.impl;

import com.hee.pea.training.service.MenuBarService;
import javafx.application.Platform;

public class MenuBarServiceImpl implements MenuBarService {

    @Override
    public void exit() {
        Platform.exit();
        System.exit(0);
    }
}
