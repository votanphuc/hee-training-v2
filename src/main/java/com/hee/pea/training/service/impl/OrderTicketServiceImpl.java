package com.hee.pea.training.service.impl;

import com.hee.pea.training.model.TradeModel;
import com.hee.pea.training.service.OrderTicketService;
import com.hee.pea.training.view.OrderTicketView;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class OrderTicketServiceImpl implements OrderTicketService {

    private static Logger LOGGER = LogManager.getLogger(OrderTicketServiceImpl.class);

    @Override
    public void addRecord(OrderTicketView view, TradeModel tradeModel) {
        LOGGER.info("Add Record");
        if (!view.getTableView().getItems().isEmpty()) {
            view.getTableView().getItems().add(tradeModel);
            return;
        }
        ObservableList<TradeModel> dataTrading = FXCollections.observableArrayList(tradeModel);
        view.getTableView().setItems(dataTrading);
    }
}
