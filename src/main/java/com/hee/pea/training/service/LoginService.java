package com.hee.pea.training.service;

public interface LoginService {

    Boolean login(String loginId, String loginPw);
}
