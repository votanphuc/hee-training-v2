package com.hee.pea.training.service.impl;

import com.hee.pea.training.service.LoginService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class LoginServiceImpl implements LoginService {

    private static final Logger LOGGER = LogManager.getLogger(LoginServiceImpl.class);

    @Override
    public Boolean login(String loginId, String loginPw) {
        return ("heevn".equals(loginId) && "h123456".equals(loginPw));
    }

}
