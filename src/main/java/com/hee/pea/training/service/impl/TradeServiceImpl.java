package com.hee.pea.training.service.impl;

import com.hee.pea.training.service.TradeService;
import com.hee.pea.training.view.TradeView;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class TradeServiceImpl implements TradeService {

    private static final Logger LOGGER = LogManager.getLogger(TradeServiceImpl.class);

    @Override
    public void clear(TradeView view) {
        LOGGER.info("Clear data in TradeView");
        view.getAccountTextField().clear();
        view.getStockTextField().clear();
        view.getbPxTextField().clear();
        view.getQtyTextField().clear();
        view.getTypeComboBox().getSelectionModel().selectFirst();
    }
}
