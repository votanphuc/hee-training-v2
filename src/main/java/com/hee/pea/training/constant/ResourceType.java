package com.hee.pea.training.constant;

import java.util.HashMap;
import java.util.Map;

public enum ResourceType {

    RESULT("Result"), //

    LABEL("Label"), //

    ;

    private String value;

    private static Map<String, ResourceType> RESOURCE_TYPE = new HashMap<String, ResourceType>();

    static {
        for (ResourceType resourceType : ResourceType.values()) {
            RESOURCE_TYPE.put(resourceType.getValue(), resourceType);
        }
    }

    ResourceType(String value) {
        this.value = value;
    }

    public static ResourceType fromValue(String value) {
        if (value == null) {
            return null;
        }
        return RESOURCE_TYPE.get(value);
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
