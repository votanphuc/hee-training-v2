package com.hee.pea.training;

import com.hee.pea.training.controller.StageController;
import com.hee.pea.training.controller.StageControllerImpl;
import com.hee.pea.training.presenter.LoginPresenter;
import com.hee.pea.training.presenter.MainPresenter;
import com.hee.pea.training.presenter.TradePresenter;
import com.hee.pea.training.view.impl.LoginViewImpl;
import com.hee.pea.training.view.impl.MainViewImpl;
import com.hee.pea.training.view.impl.TradeViewImpl;
import com.hee.pea.training.viewmodel.LoginViewModel;
import com.hee.pea.training.viewmodel.MainViewModel;
import com.hee.pea.training.viewmodel.TradeViewModel;
import javafx.application.Application;
import javafx.stage.Stage;

public class MyApplication extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {

        LoginPresenter loginPresenter = new LoginPresenter(new LoginViewImpl(), new LoginViewModel());

        StageController stageController = new StageControllerImpl();
        stageController.start(primaryStage, loginPresenter);

    }

    public static void main(String[] args) {
        launch(args);
    }
}