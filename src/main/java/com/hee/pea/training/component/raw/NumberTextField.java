package com.hee.pea.training.component.raw;

import com.google.common.base.CharMatcher;
import com.google.common.base.Strings;
import com.hee.pea.training.helper.NumberFormatHelper;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.event.EventHandler;
import javafx.scene.control.TextFormatter;
import javafx.scene.input.KeyEvent;
import javafx.util.StringConverter;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.math.BigDecimal;
import java.util.function.UnaryOperator;

public class NumberTextField extends BaseTextField {

    private static Logger LOGGER = LogManager.getLogger(NumberTextField.class.getName());
    StringConverter<Number> numberConverter = new StringConverter<Number>() {

        @Override
        public String toString(Number object) {
            if (object == null) {
                return "-";
            }
            return object.toString();
        }

        @Override
        public Number fromString(String string) {
            if (Strings.isNullOrEmpty(string) || string.equals("-")) {
                return BigDecimal.ZERO;
            }
            return new BigDecimal(StringUtils.replace(string, ",", ""));
        }
    };
    EventHandler<KeyEvent> quantityEventHandler = event -> {
        if (!event.isConsumed() && !CharMatcher.digit().matchesAllOf(event.getCharacter())) {
            event.consume();
        }
    };
    EventHandler<KeyEvent> currencyEventHandler = event -> {
        if (!event.isConsumed() &&
                (CharMatcher.is('.').countIn(event.getCharacter()) > 1) || !CharMatcher.digit().matchesAllOf(StringUtils.replace(event.getCharacter(), ".", ""))) {
            event.consume();
        }
    };
    UnaryOperator<TextFormatter.Change> changeFilter = change -> {
        if (!change.isContentChange()) {
            return change;
        }

        if (StringUtils.replace(change.getControlNewText(), ",", "").length() > maxLength) {
            return null;
        }
        return change;
    };
    UnaryOperator<TextFormatter.Change> currencyFilter = change -> {
        if (!change.isContentChange()) {
            return change;
        }

        String string = change.getControlNewText();
        int dotIndex = string.indexOf(".");

        if (dotIndex != -1) {
            string = string.substring(0, dotIndex);
        }

        if (StringUtils.replace(string, ",", "").length() > maxLength) {
            return null;
        }

        return change;
    };
    private NumberTextFieldType numberTextFieldType;
    private ObjectProperty<Number> value = new SimpleObjectProperty<>();

    public NumberTextField(NumberTextFieldType numberTextFieldType) {
        this("", numberTextFieldType);
    }

    public NumberTextField(String text, NumberTextFieldType numberTextFieldType) {
        this(text, numberTextFieldType, false);
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    public NumberTextField(String text, NumberTextFieldType numberTextFieldType, boolean isAllowNull) {
        super(text);

        this.numberTextFieldType = numberTextFieldType;

        switch (numberTextFieldType) {
            case QUANTITY:
                this.addEventFilter(KeyEvent.KEY_TYPED, quantityEventHandler);
                if (isAllowNull) {
                    this.setTextFormatter(new TextFormatter(NumberFormatHelper.quantityWithNullConverter(), null, changeFilter));
                } else {
                    this.setTextFormatter(new TextFormatter(NumberFormatHelper.quantityConverter(), BigDecimal.ZERO, changeFilter));
                }
                break;
            case ANY:
            case CURRENCY:
                this.addEventFilter(KeyEvent.KEY_TYPED, currencyEventHandler);
                if (isAllowNull) {
                    this.setTextFormatter(new TextFormatter(NumberFormatHelper.currencyWIthNullConverter(), null, currencyFilter));
                } else {
                    this.setTextFormatter(new TextFormatter(NumberFormatHelper.currencyConverter(), BigDecimal.ZERO, currencyFilter));
                }
                break;
            default:
                break;
        }

        this.textProperty().bindBidirectional(value, numberConverter);
    }

    public NumberTextFieldType getNumberTextFieldType() {
        return numberTextFieldType;
    }

    public void setNumberTextFieldType(NumberTextFieldType numberTextFieldType) {
        this.numberTextFieldType = numberTextFieldType;
    }

    public final ObjectProperty<Number> valueProperty() {
        return this.value;
    }

    public final Number getValue() {
        return this.valueProperty().get();
    }

    public final void setValue(final Number value) {
        this.valueProperty().set(value);
    }

    public static enum NumberTextFieldType {
        ANY, QUANTITY, CURRENCY
    }

}
