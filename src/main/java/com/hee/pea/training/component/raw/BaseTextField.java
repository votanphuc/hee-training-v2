package com.hee.pea.training.component.raw;

import javafx.scene.control.TextField;

public abstract class BaseTextField extends TextField {
	
	protected static double DEFAULT_TEXT_FIELD_HEIGHT = 27.5;
	
	protected static int MAX_LENGTH = 10;
	
	protected int maxLength = MAX_LENGTH;
	
	public BaseTextField(String text) {
		super(text);
		initLayout();
	}
	
	public void initLayout() {
		this.minWidth(USE_COMPUTED_SIZE);
		this.minHeight(DEFAULT_TEXT_FIELD_HEIGHT);
		this.prefWidth(USE_COMPUTED_SIZE);
		this.prefHeight(DEFAULT_TEXT_FIELD_HEIGHT);
		this.maxWidth(Double.MAX_VALUE);
		this.maxHeight(DEFAULT_TEXT_FIELD_HEIGHT);
	}

	public int getMaxLength() {
		return maxLength;
	}

	public void setMaxLength(int maxLength) {
		this.maxLength = maxLength;
	}
}
