package com.hee.pea.training.config;

import com.hee.pea.training.presenter.TradePresenter;
import com.hee.pea.training.view.impl.TradeViewImpl;
import com.hee.pea.training.viewmodel.TradeViewModel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class PresenterConfig {
    private static Logger LOGGER = LogManager.getLogger(PresenterConfig.class.getName());

    public TradePresenter tradePresenter() {
        return new TradePresenter(new TradeViewImpl(), new TradeViewModel());
    }
}
